##### _Capítulo 1_
## _O ÁBACO_

O ábaco foi a primeira calculadora da história. Criado provavelmente pelos chineses no século 6 a.C, esse instrumento dispunha de fios paralelos e arruelas deslizantes, capazes de realizar contas de adição e subtração. Essa invenção revolucionou a matemática, pois acabou sendo o principal mecanismo de cálculo durante os 24 séculos seguintes.

A palavra "ábaco" veio do grego abakos, derivado de abax: tábua de cálculos ou soroban (算盤の) em japonês.

O primeiro ábaco foi provavelmente construído numa pedra lisa coberta por areia ou pó, esse provavelmente teve sua origem na Mesopotâmia há mais de 5500 anos a.C., foi considerado como uma extensão do ato de se contar nos dedos. O que quer dizer que ele foi muito importante pra época.

Varias civilizações como a Roma, Babilónia, Grécia, Egito, Rússia, Japão, Mesopotâmia e entre outras fizeram a sua versão do ábaco. Por isso existem muitas diferenças entre cada versão construída. A origem do ábaco de contar com bastões é obscuro, mas a Índia, a Mesopotâmia ou o Egito são vistos como prováveis pontos de origem.
Dentre os muitos desenvolvidos; o mais popular utiliza uma combinação de dois números-base (2 e 5) para representar números decimais. Mas os mais antigos ábacos usavam números sexagesimais representados por fatores de 5, 2, 3 e 2 por cada dígito.

O instrumento é basicamente uma moldura retangular com hastes em paralelo, onde cada um correspondente ao múltiplo de dez, uma posição digital (unidades, dezenas,...) e nos quais estão os elementos de contagem (fichas, bolas, contas,...) que podem fazer-se deslizar livremente. Ele é utilizado ainda hoje para ensinar às crianças as operações de somar e subtrair.


***Link para um ábaco antigo:*** [LINK](https://www.google.com/url?sa=i&url=https%3A%2F%2Fescola.britannica.com.br%2Fartigo%2F%25C3%25A1baco%2F480503&psig=AOvVaw32JxkrAGojVCYxAeLtC578&ust=1634508690944000&source=images&cd=vfe&ved=0CAsQjRxqFwoTCOiSxsH5z_MCFQAAAAAdAAAAABAD)

***Link para um ábaco mais usado atualmente:***   [LINK](https://www.casadaeducacao.com.br/abaco-com-10-colunas--carimbras.608.html)
-  Exemplo de cálculo
Ao iniciar um calculo, todas as hiperbolas devem ser colocadas no extremo superior e, as hipobolas no extremo inferior. A vareta do meio é o local onde se irão colocar as bolas usadas no calculo. As bolas que permanecem inativas devem ser colocadas nas laterais.
O cálculo ocorre da esquerda para direita, ou na haste mais alta envolvida em seu cálculo. Assim, se tiver 548 e desejar somar 637, primeiro colocará 548 na calculadora. Daí, adiciona 6 ao 5. Segue a regra ou padrão 6 = 10 - 4 por remover o 5 na vara das centenas e adicionar 1 na mesma vara (-5 + 1 = -4) daí, adicione uma das contas de milhares à vara à esquerda. Daí, passa a somar o três ao quatro, o sete ao oito, e no ábaco aparecerá a resposta: 1.185.

Referência das informações:[https://pt.wikipedia.org/wiki/%C3%81baco]

##### _Capítulo 2_

## _La pascaline_

La pascaline foi a primeira calculadora mecânica do mundo, planejada por Blaise Pascal em 1642.

Originalmente, Pascal pretendia construir uma máquina que realizasse as quatro operações fundamentais, mas apenas conseguia fazer diretamente operações de adição e subtração. As operações de multiplicação e divisão podiam ser feitas por repetição.

Blaise Pascal motivou-se a criar esta máquina porque seu pai era contador e precisava de ajuda com cálculos mais avançados. Pascal então, com seu conhecimento em física e em matemática, criou uma máquina com um engenhoso sistema de engrenagens que fazia contas de adição e subtração. Quando a engrenagem virasse à direita era feita a adição, quando virasse à esquerda era feita a subtração dos valores armazenados em 2 ou 3 engrenagens anteriores.

***Link para La pascaline:***   [LINK](https://pt.wikipedia.org/wiki/La_pascaline#/media/Ficheiro:Arts_et_Metiers_Pascaline_dsc03869.jpg)

Referência das informações:[https://pt.wikipedia.org/wiki/La_pascaline]

A Pascaline em seu interior tinha como elemento essencial uma roda dentada construída com 10 "dentes". Cada "dente" corresponde a um algarismo, de 0 a 9. A primeira roda da direita corresponde às unidades, a segunda a sua esquerda corresponde às dezenas, a seguinte às centenas e assim sucessivamente.

Um mecanismo muito simples construído com uma "garra" resolvia o problema de transporte, cada vez em que o algarismo de uma das rodas passava de nove a zero, fazendo com que as rodas vizinhas fossem arrastadas, deslocando-se um dente, para frente ou para trás, dependo da operação matemática.

Pascal recebeu a patente do rei da França para criar mais Pascalines e vender no mercado. Ele construiu 50 versões dessa calculadora, mas ela não teve muita aceitação no mercado.

Antes disso, ouve algumas boas tentativas de criar uma calculadora mecânica.
Como o **relógio calculador** mas Infelizmente o único exemplar dessa calculadora (ainda incompleto) foi destruído em um incêndio, Mas na década de 60, foram encontradas as anotações do projeto dessa calculadora, possibilitando sua reprodução, atribuindo a Wilhelm Schickard a invenção da calculadora.

***Link para o relógio calculador:***   [LINK](https://1.bp.blogspot.com/-4DNh4tUIXeA/UEt_1v42bCI/AAAAAAAAAXQ/B_Ez41fHnaM/s1600/replica+da+calculadora+wihelm+.jpg)

O relógio calculador foi feito em 1623. Essa calculadora era capaz de somar, subtrair, dividir e multiplicar números ate 6 dígitos, indicando o resultado através de um toque de sino.

Em 1672, o matemático Gottfried Wilhelm Von Leibniz baseando se na calculadora de Pascal, construiu a **stepped reckoner**. A stepped reckoner era uma calculadora mecânica que fazia as quatro operações básicas e a raiz quadrada dos números. Mas como Leibniz não chegou a terminar a calculadora, ela ainda apresentava alguns erros ao fazer cálculos de divisão e raiz quadrada.

***Link para stepped reckoner:***   [LINK](https://4.bp.blogspot.com/-3TPGPNu6hvo/Uogg9vfEYjI/AAAAAAAABz4/Uzl4Sx8Osx0/s1600/stepped-reckoner-de-leibniz.jpg)

E com o passar dos séculos foram surgindo mais calculadoras  mecânicas. Algumas muito semelhantes entre si, com formatos diferentes, inovando em detalhes específicos e assim evoluindo. Cada vez tendo mais uma aparência de computador.

Em 1822, o matemático inglês Charles Babbage constrói um modelo da ***Difference Engine*** (em português, maquina da diferença), que era uma máquina capaz de resolver equações polinômicas através de um método de diferenças infinitas, o qual evitava a necessidade de multiplicação e divisão, que possibilitava a construção de tabelas logarítmicas.

A Difference Engine era capaz de receber os dados, processá-los, guardá-los e depois mostrá-los, por isso, ela também é conhecida como um dos primeiros computadores da história.

***Link para Difference Engine:***   [LINK](https://3.bp.blogspot.com/-yo87QihN50g/UEvLDOzZwCI/AAAAAAAAAYI/-AXCgOYNeTA/s1600/800px-Babbage_Difference_Engine.jpg)

Referência das informações:[https://www.engquimicasantossp.com.br/2012/09/historia-da-calculadora.html]








