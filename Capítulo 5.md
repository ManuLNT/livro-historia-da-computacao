##### _Capítulo 5_

## _Celular e conexão_

Hoje na terceira década dos anos 2000, uma parcela muito grande da populção do mundo está conectada na internet. Ela foi  criada em 1969, nos Estados Unidos. Chamada de Arpanet, tinha como função interligar laboratórios de pesquisa. Naquele ano, um professor da Universidade da Califórnia passou para um amigo em Stanford o primeiro e-mail da história.Essa rede pertencia ao Departamento de Defesa norte-americano. O mundo vivia o auge da Guerra Fria. Ela foi criada para comunicação, é pra isso que ela é usada hoje, mas de varias formas diferentes.

Aconteceu a criação do sistema operacional ***Android***baseado no núcleo Linux, desenvolvido por um consórcio de desenvolvedores conhecido como Open Handset Alliance. O  android é o sistema operacional principal nos microcomputadores conhecidos hoje como ***smartphones,*** eles tem acesso a internet, e nela, as pessoas hoje se comunicam através de sites e redes sociais, divulgam seu trabalho e até trabalham com a internet. 

>A internet se tornou uma das principais formas pra se consumir  
conteúdo, como filmes, livros e todo tipo de entretenimento que em alguns casos surgiu por causa da internet.
>E assim sendo  a internet também é uma forma de sustentar o trabalho de muitas pessoas nas mais diferentes areas.

Por mais que os celulares sejam os "tipos de computadores "mais usados e consumidos ,os PCs (Personal Computer)  e video gamestams também tem muita força no mercado atual. Ambos se conectando na internet . Os consoles de jogos eletrônicos em geral também são uma das formas de criação de conteúdo nos dias de hoje, tendo um consumo muito alto.

***Lista de consoles de jogos eletrônicos mais vendidos:[LINK](https://pt.wikipedia.org/wiki/Lista_de_consoles_de_jogos_eletr%C3%B4nicos_mais_vendidos)***

Hoje todo mundo tem um celular, eles antigamente eram apenas usados para chamadas telefônicas através de ondas eletromagnéticas.
Na União Soviética, o primeiro celular foi desenvolvido em 1955 por Leonid Kupriyanovich. Ele pesava 1,2 quilogramas e tinha alcance de 1,5 quilômetro. 

>Os celulares com o passar do tempo foram tomando caminhos e formas diferentes, hoje eles tem funções que vão muito além de chamadas telefônicas, podem ser usados para jogar videogames, pra adquirir informação ou até pra pedir comida de forma direta(sem ser necessária uma ligação).

Há diferentes tecnologias para a difusão das ondas eletromagnéticas nos telefones móveis, baseadas na compressão das informações ou na sua distribuição: na primeira geração (1G) (a analógica, desenvolvida no início da década de 1980), com os sistemas [NMT](https://pt.wikipedia.org/wiki/Nordic_Mobile_Telephone) e [AMPS](https://pt.wikipedia.org/wiki/AMPS); na segunda geração (2G) (digital, desenvolvida no final da década de 1980 e início da década de 1990): [GSM](https://pt.wikipedia.org/wiki/GSM), [CDMA](https://pt.wikipedia.org/wiki/CDMA) e [TDMA](https://pt.wikipedia.org/wiki/TDMA); na segunda geração e meia (2,5G) (uma evolução à 2G, com melhorias significativas em capacidade de transmissão de dados e na adoção da tecnologia de pacotes e não mais comutação de circuitos), presente nas tecnologias [GPRS](https://pt.wikipedia.org/wiki/General_Packet_Radio_Service), [EDGE](https://pt.wikipedia.org/wiki/Enhanced_Data_rates_for_GSM_Evolution), [HSCSD](https://pt.wikipedia.org/wiki/High-Speed_Circuit-Switched_Data) e [1xRTT](https://pt.wikipedia.org/wiki/CDMA2000); na terceira geração (3G)  sendo digital, com mais recursos, em desenvolvimento desde o final dos anos 1990, como [UMTS](https://pt.wikipedia.org/wiki/Universal_Mobile_Telecommunications_System) e [EVDO](https://pt.wikipedia.org/wiki/Evolution-Data_Optimized); na terceira geração e meia (3,5G), como [HSDPA](https://pt.wikipedia.org/wiki/HSDPA#:~:text=High%2DSpeed%20Downlink%20Packet%20Access,em%20uma%20banda%20de%205MHz.), [HSPA](https://pt.wikipedia.org/wiki/HSPA#:~:text=High%20Speed%20Packet%20Access%20(HSPA,desempenho%20dos%20protocolos%20existentes%20WCDMA.)) e [HSUPA](https://www.gta.ufrj.br/ensino/eel879/Anos-anteriores/2008-2/trabalhos_vf/marden/HSUPA(HighSpeedUplinkPacketAcess).html). Depois da implementação do 4G estando baseada totalmente em [IP](https://pt.wikipedia.org/wiki/Endere%C3%A7o_IP), e hoje começando o 5G trazendo o MMWAVE, BEAMFORMING e FATIAMENTO DE REDE.

***Sobre o 5G: [LINK](https://www.redhat.com/pt-br/topics/5g-networks)***

***Evolução dos celulares: [LINK](https://melhorplano.net/tecnologia/evolucao-do-celular)***

***Vídeo sobre a evolução dos celulares:[LINK](https://www.youtube.com/watch?v=LM2wSf5NL_A)***


Referência das informações:https://pt.wikipedia.org/wiki/Hist%C3%B3ria_da_Internet

https://pt.wikipedia.org/wiki/Telefone_celular#:~:text=Os%20primeiros%20prot%C3%B3tipos%20de%20telefones,desenvolver%20um%20modelo%20em%201956.&text=Em%
