##### _Capítulo 3_ 

## _ENIAC_

Com o desenvolvimento do uso de equipamentos eletrônicos de varios tipos, a pedido do exército dos Estados Unidos para seu laboratório de pesquisa balística, em 1946 foi revelado o primeiro computador eletrônico digital de larga escala do mundo, o ENIAC (Computador e Integrador Numérico Eletrônico).
Divulgado pelos pesquisadores norte-americanos John Eckert e John Mauchly, da Electronic Control Company.

***Link para ENIAC*:**  [LINK](https://pt.wikipedia.org/wiki/ENIAC#/media/Ficheiro:ENIAC_Penn1.jpg)

O ENIAC tinha o  30 toneladas de peso que ocupava uma área de 180 m² de construída. Sua produção custou nada menos do que US$ 500 mil na época, o que hoje representaria aproximadamente US$ 6 milhões e a máquina contava com um hardware equipado com 70 mil resístores e 18 mil válvulas de vácuo que em funcionamento consumiam grandes  200 mil watts de energia.

Quem exatamente o inventou não é algo que se sabe com muita precis,há quem diga que foi Charles Babbage, partindo de sua ***Máquina Analítica;*** outros, que foi Alan Turing, que definiu o que é um computador; e outros, que foram ou dois, ou mesmo nenhum. Mas provavelmente seria muito mais difícil o seu desenvolvimento sem o investimento do exército dos Estados Unidos, que estava passando pela Segunda Guerra Mundial.  

Na Segunda Guerra Mundial ele era usado para   cálculos balísticos de alto nível, ou seja, na configuração de armas como uma bomba de Hidrogênio por exemplo.
O ENIAC era programado através de milhares de interruptores, podendo cada um deles assumir o valor 1 ou 0 consoante o interruptor estava ligado ou desligado.
Para o programar era necessário uma grande quantidade de pessoas que percorriam as longas filas de interruptores dando ao ENIAC as instruções necessárias para computar, ou seja, calcular.

***Link para  vídeo sobre o ENIAC :***[LINK](https://www.youtube.com/watch?v=k4oGI_dNaPc&t=47s)

Depois de 10 anos de uso o ENIAC se tornou obsoleto,  tendo sido desativado no dia 2 de outubro de 1955 e posteriormente desmontado. Hoje encontram-se peças do ENIAC por muitos museus do mundo, incluindo o Smithsonian em Washington D.C e no local preciso onde foi construído, na Moore School for Electrical Engineering da Universidade da Pensilvânia.
Estando no final de sua carreira, um rival com o dobro da capacidade custava o equivalente a US$ 200 mil e tinha apenas 10% de seu tamanho.

Referência das informações: [https://pt.wikipedia.org/wiki/ENIAC]
[https://tecnoblog.net/56910/eniac-primeiro-computador-do-mundo-completa-65-anos/]



##### _Capítulo 4_ 

## _Desenvolvimento dos computadores_
Com o passar do tempo mais computadores foram surgindo como o EDVAC, o ORDVAC, ;SEAC e UNIVAC. Isso por volta da década de 50(1950).Em 1955, um computador pesava 3 toneladas e consumia 50 kW de potência, tendo um custo de US$ 200 000,00. Uma máquina destas podia realizar 50 multiplicações por segundo.

Com o tempo, os transístores passaram a ser a base da electrónica, seguindo-se a VLSI (Very Large Scale Integration), ou seja, a construção de circuitos cada vez menores por forma a que pudessem ser mais leves e despender menos energia, por terem menos superfície para a dissipação de energia por calor. Esta miniaturização permitiu que se tivesse a mesma capacidade de cálculo de um ENIAC na palma de uma mão. A diminuição do tamanho fez também diminuir a quantidade de energia necessária e o custo caiu com a produção em série dos novos processadores.Então os computadores foram ficando mais compactos e ainda com a capacidade cada vez maior.  

 Foram sendo feitos vários tipos de sistema operacional, primeiro só grandes empresas tinham condições de ter um computador, que era uma grande novidade para a época. Na década de 80 o consumo continuava a crescer e contínua a crescer muito nos dias de hoje.
 
 Link para computador dos anos 80(IBM PC 5150):[LINK](https://pt.wikipedia.org/wiki/IBM_PC#/media/Ficheiro:IBM_PC_5150.jpg)
 
Ainda sim, o mercado de calculadoras simples era bem forte. A primeira calculadora eletrônica do mundo foi ***14-A*** produzida pela Casio em 1957, A 14-A efetuava operações de adição, subtração, multiplicação e divisão com até 14 dígitos. A ***calculadora cientifica*** veio depois com forte uma grande quantidade de fabricantes, e a partir dai o termo ***guerra das calculadoras***, devido a grande competição que acontecia no mercado.
As calculadoras cientificas vem sempre evoluído, ganhando funções novas a cada ano que passa, até os dias de hoje.

***Link para 14-A:***[LINK](https://4.bp.blogspot.com/-FlrBeiH1W3U/UogieYozOiI/AAAAAAAAB0Y/bRLjtMUVXv8/s1600/calculadora-casio-14a.png)

***Link para calculadoras cientificas:***
[Texas TI-Nspire CAS
](https://2.bp.blogspot.com/-RN7cuFkcXnw/UEwBto7OksI/AAAAAAAAAZo/YI349DsuqA0/s1600/TI-NspireCAS.jpg) [HP-35B calculator da Hp
](https://4.bp.blogspot.com/-Kp-ZOVPh2RQ/UogjCjxSMmI/AAAAAAAAB0o/WRfVUEwj13w/s1600/HP-35B-calculator-da-Hp.png) [Calculadora modelo 9100A de 1968
](https://3.bp.blogspot.com/-g1UdvuKeetU/Uogit8GMwdI/AAAAAAAAB0g/HsuRR6Y-7EE/s1600/calculadora-modelo-9100A-de-1968.png)

##### _Próximo do presente_
A criação de sistemas operacionais como o ***Windows*** em 1985 e ***Linux*** em 1991 trouxe um ar de inovação para os computadores. Assim como a criação dos video games(jogo eletrônicos) como o ***Magnavox Odyssey***, que deu início ao que chamamos de consoles, no ano de 1972. O mercado de computadores com sistemas operacionais foi se desenvolvendo com atualizações constantes até os dias de hoje, assim como no mercado de consoles.Existem varios exemplos de  produtos muito mais desenvolvidos e comercializados nos dias atuais como o ***Windows 11*** e o ***Playstation 5***.São tão comercializados que acabam fazendo um papel muito importante na vida de muitas pessoas pelo mundo todo, que foi praticamente conectado através da internet.


***Link para Magnavox Odyssey***:[LINK](https://pt.wikipedia.org/wiki/Magnavox_Odyssey#/media/Ficheiro:Magnavox-Odyssey-Console-Set.jpg)

***Link para Windows 11***:[LINK](https://pt.wikipedia.org/wiki/Microsoft_Windows#/media/Ficheiro:Windows-11.jpg)

***Link para Playstation 5***:[LINK](https://pt.wikipedia.org/wiki/PlayStation_5#/media/Ficheiro:PlayStation_5_and_DualSense_with_transparent_background.png)

***Link para Linux***:[LINK](https://pt.wikipedia.org/wiki/Linux#/media/Ficheiro:KDE-Desktop-5.jpg)

Informações adicionais sobre consoles: https://www.copeltelecom.com/site/blog/a-evolucao-dos-consoles-de-videogame/

Referência das informações: https://www.engquimicasantossp.com.br/2012/09/historia-da-calculadora.html





