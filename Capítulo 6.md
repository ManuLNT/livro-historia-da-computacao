##### _Capítulo 6_

## _Inteligência Artificial_

Com o avanço dos computadores e de outras tecnologias, ferramentas e ideias que pareciam ser impossíveis, hoje, estão em desenvolvimento e aperfeiçoamento. Como é o caso da Inteliência Artificial, também conhecida como IA que é um conceito de um sistema, máquina ou tecnologia que realizam atividades de modo similar a um humano, podendo raciocínar, perceber, analizar para tomar uma decisão.

Definida por Andreas Kaplan e Michael Haenlein como “uma capacidade do sistema para interpretar corretamente dados externos, aprender a partir desses dados e utilizar essas aprendizagens para atingir objetivos e tarefas específicas através de adaptação flexível” , a inteligência artificial tem como objetivo execultar funções que, caso fossem execultadas por um ser humano , seriam consideiradas inteligentes.

O desenvolvimento dessa tecnologia começou depois da segunda guerra, com Alan Turing em seu artigo "Computing Machinery and intelligence", este desenvolvimento que continua até os dias atuais, com empresas como Apple, google,microsoft.. que possuem AIs "comerciais" popularmente  chamada de assistentes virtuais que tem como objetivo facilitar a vida de quem "as usa", anotando,repondendo perguntas e dando informações ao usuário.

Assistentes virtuais:[LINK](
https://convexnet.com.br/assistentes-virtuais-alexa-google-assistente-e-siri/) 

Referências: https://pt.wikipedia.org/wiki/Intelig%C3%AAncia_artificial ,https://www.totvs.com/blog/inovacoes/o-que-e-inteligencia-artificial/, https://pt.wikipedia.org/wiki/Intelig%C3%AAncia_artificial
